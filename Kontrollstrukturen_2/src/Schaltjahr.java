import java.util.Scanner; // Import der Klasse Scanner 

public class Schaltjahr {

		public static void main(String[] args) {
	        // AB Auswahlstrukturen � Aufgabe 8: Schaltjahr
	        System.out.print("Gib eine Jahreszahl ein (YYYY): "); 
	        Scanner scanner = new Scanner(System.in);
	        int year = scanner.nextInt();

	        if(year < 1582) {
	            if(year % 4 == 0) {
	                System.out.println("\n" + year + " ist ein Schaltjahr");
	            }else {
	                System.out.print("\n" + year + " ist kein Schaltjahr"); 
	            }
	        }else if(year >= 1582) {
	            if((year % 4 == 0) && (year % 100 != 0)) {
	                System.out.println("\n" + year + " ist ein Schaltjahr"); 
	            }else if(year % 400 == 0) {
	                System.out.println("\n" + year + " ist ein Schaltjahr");
	            }else if(year % 100 == 0) {
	                System.out.println("\n" + year + " ist kein Schaltjahr"); 
	            }
	            else {
	                System.out.println("\n" + year + " ist kein Schaltjahr"); 
	            }
	        }

	        scanner.close(); 
		}
}
