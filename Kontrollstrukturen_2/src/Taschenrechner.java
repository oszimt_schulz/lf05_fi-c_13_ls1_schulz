import java.util.Scanner; // Import der Klasse Scanner 

public class Taschenrechner {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	Scanner myScanner = new Scanner(System.in);

	System.out.print("Bitte geben Sie ihre erste Zahl ein "); 
    double zahl1 = myScanner.nextDouble();
    
    System.out.print("Bitte geben Sie ihre zweite Zahl ein "); 
    double zahl2 = myScanner.nextDouble();
    
    System.out.print("Bitte geben Sie ihren Rechnungsoperator an (+,-,*,/) "); 
    char operationen = myScanner.next().charAt(0);
    
    //If schleifen mit den entsprechenden berechnungen
    if (operationen == '+') {
    	double ergebnis = zahl1 + zahl2;
    	System.out.print("Dein Ergebnis ist " + ergebnis);
    }
    if (operationen == '-') {
    	double ergebnis = zahl1 - zahl2;
    	System.out.print("Dein Ergebnis ist " + ergebnis);
    }
    if (operationen == '*') {
    	double ergebnis = zahl1 * zahl2;
    	System.out.print("Dein Ergebnis ist " + ergebnis);
    }
    if (operationen == '/') {
    	double ergebnis = zahl1 / zahl2;
    	System.out.print("Dein Ergebnis ist " + ergebnis);
    }
    else {
    	System.out.println("Ergebnis nicht ermittelbar durch inkorrekte Angaben");
    }
}
}
	