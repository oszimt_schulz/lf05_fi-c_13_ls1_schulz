import java.util.Scanner;

public class OhmischesGesetz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Welche Gr��e soll berechnet werden? U,I oder R "); 
	    char groe�e = myScanner.next().charAt(0);
	     
	    //If schleifen mit den entsprechenden berechnungen
	    if (groe�e == 'R') {
	    	System.out.print("Bitte gebe einen Wert f�r U ein "); 
		    double spannung = myScanner.nextDouble();
		    System.out.print("Bitte gebe einen Wert f�r I ein "); 
		    double stromstearke = myScanner.nextDouble();
		    
		    double widerstand = spannung / stromstearke;
		    System.out.print("Der Widerstand betr�gt: " + widerstand + " Ohm"); 
		    }
	    
	    else if (groe�e == 'I') {
	    	System.out.print("Bitte gebe einen Wert f�r U ein "); 
		    double spannung = myScanner.nextDouble();
		    System.out.print("Bitte gebe einen Wert f�r R ein "); 
		    double widerstand = myScanner.nextDouble();
		    
		    double stromstearke = widerstand * spannung;
		    System.out.print("Die Stromst�rke betr�gt: " + stromstearke + " Ampere"); 
		    }
	    
	    else if (groe�e == 'U') {
	    	System.out.print("Bitte gebe einen Wert f�r I ein "); 
		    double stromstearke = myScanner.nextDouble();
		    System.out.print("Bitte gebe einen Wert f�r R ein ");
		    double widerstand = myScanner.nextDouble();
		    
		    double spannung = widerstand * stromstearke;
		    System.out.print("Die Spannung betr�gt: " + spannung + " Volt"); 
		    }
	    
	    else {
	    	System.out.println("Bitte trage eine korrekte Gr��e ein!");
	    }
	}
}
		