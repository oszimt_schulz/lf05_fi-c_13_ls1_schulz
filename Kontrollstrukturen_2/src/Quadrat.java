import java.util.Scanner; // Import der Klasse Scanner 


public class Quadrat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine Zahl ein "); 
	    int zaehler = myScanner.nextInt();
	    for (int zeile = 0; zeile < zaehler; zeile++) {
			if (zeile == 0 || zeile == zaehler - 1) {
				for (int spalte = 0; spalte < zaehler; spalte++) {
					System.out.print("*");
					}
				
				}

		    else {
		    	System.out.print("*");
		    	for (int space = 0; space < zaehler - 2; space++) {
		    		System.out.print(" ");
		    		}
		    	System.out.println(" *");
		    	}
			}
		}
}