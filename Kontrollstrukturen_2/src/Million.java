import java.util.Scanner;	

public class Million {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner myScanner = new Scanner(System.in);
		boolean start = true;
		
		while(start == true) {
			System.out.print("Bitte geben Sie ihre Einlage an: "); 
		    double kapital = myScanner.nextDouble();
		    
		    System.out.print("Bitte geben Sie den Zinssatz an: "); 
		    double zinssatz = myScanner.nextDouble();
		    int jahre = 0;
		    
		    while (kapital < 1000000) {
		    	double rechnung = (kapital*zinssatz)/100;
		    	double ergebnis = kapital + rechnung;
		    	kapital = ergebnis;
		    	jahre = jahre + 1;
		    }
		    	    
		    System.out.println("Die Benötigten Jahre bis zur Million sind:" + jahre);
		   
		    System.out.println("Wollen Sie einen neu loslegen ? (J/N) ");
		    char restart = myScanner.next().charAt(0);
		    
		    if (restart == 'N') {
		    	start = false;
		    	}
		    else if (restart == 'J') {
		    	start = true;
		    }
		    else{
		    	start = false;
			    System.out.print("Keine Korrekte Eingabe"); 
		    }
		}
	}
}