import java.util.Scanner; // Import der Klasse Scanner 

public class BMI {

	private static final int M = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie ihr Gewicht in kg an: "); 
	    double gewicht = myScanner.nextDouble();
	    
	    System.out.print("Geben Sie ihr Körpergröße in cm an: "); 
	    double koerpergroeße = myScanner.nextDouble();
	    
	    System.out.print("Geben Sie ihr Geschlecht an (M/W) "); 
	    char geschlecht = myScanner.next().charAt(0);	    
	    
	    double umrechnunginmeter = koerpergroeße /100;
	    double umrechnunginquadratmeter = umrechnunginmeter * umrechnunginmeter;
	    double berechnungBMI = gewicht / umrechnunginquadratmeter;
	   
	    System.out.printf("\n%s%.2f", "Du hast ein BMI von ", berechnungBMI);
	    
	    if (geschlecht == 'M') {
	    	if (berechnungBMI < 20) {
		    	System.out.println("\nKlassifikation: Untergewicht");
		    }
	    	else if (berechnungBMI > 20 && berechnungBMI < 25) {
		    	System.out.println("\nKlassifikation: Normalgewicht");
		    }
		    else if (berechnungBMI > 25) {
		    	System.out.println("\nKlassifikation: Übergewicht");
		    	}
	    }
	    
	    else if (geschlecht == 'W') {
	    	if (berechnungBMI < 19) {
	    		System.out.println(" \nKlassifikation: Untergewicht");
	    	}
	    	else if (berechnungBMI > 19 && berechnungBMI < 24) {
	    		System.out.println(" \nKlassifikation: Normalgewicht");
	    	}
	    	else if (berechnungBMI > 24) {
	    		System.out.println(" \nKlassifikation: Übergewicht");
	    	}
	    }
	    
	    else {
	    	System.out.println("\nKlassifikation: Nicht ermittelbar durch inkorrekte Angaben");
	    }
	    

	}
}