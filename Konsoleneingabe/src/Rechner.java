import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt(); 
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
    
    System.out.print("Anzahl der Tickets: "); 

 // Die Variable anzahl speichert die dritte Eingabe 
    int anzahl = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    int ergebnis1 = zahl1 + zahl2 * anzahl;
    int ergebnis2 = zahl1 - zahl2 * anzahl;  
    int ergebnis3 = zahl1 * zahl2 * anzahl;  
    double ergebnis4 = zahl1+ 0.0 * anzahl;
    double ergebnis5 = ergebnis4 / zahl2 * anzahl;

     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis1);
    
    System.out.print("\n\n\nErgebnis der Subtraction lautet: "); 
    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis2);  
    
    System.out.print("\n\n\nErgebnis der Multiplikation lautet: "); 
    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis3);  
    
    System.out.print("\n\n\nErgebnis der Division lautet: "); 
    System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis5);  
 
    myScanner.close(); 
     
  }    
}