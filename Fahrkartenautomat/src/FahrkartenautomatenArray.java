/*5.3 Das Fahrkartenarray Aufgabe1= 
	Der Vorteil ist eine besser Code�bersicht und eine vereinfachte �nderbarkeit.
  5.3 Das Fahrkartenarray Aufgabe3=
  Der Vorteil ist, dass weitere Eintr�ge einfach in das Array geschrieben werden kann, ohne das es noch weitere Anpassungen von n�ten sind.
	*/


import java.util.Scanner;

public class FahrkartenautomatenArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	
    do {
       double zuZahlenderBetrag; 
       double r�ckgabebetrag;
       zuZahlenderBetrag = fahkartenbestellungErfassen();
       
       // Geldeinwurf
       r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       fahrkartenAusgeben();

       // R�ckgeldberechnung und -Ausgabe
       rueckgeldAusgeben(r�ckgabebetrag);
       }      
       while (restart() == true);
    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
 
    }
    
    public static double fahkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double ticketpreis = 0;
    	int wahl;
    	int anzahlTickets;
    	String[] fahrkartenbezeichnung = {"1. Einzelfahrschein Berlin AB", "2. Einzelfahrschein Berlin BC" , "3. Einzelfahrschein Berlin ABC", "4. Kurzstrecke", "5. Tageskarte Berlin AB", "6. Tageskarte Berlin BC", "7. Tageskarte Berlin ABC", "8. Kleingruppen-Tageskarte Berlin AB", "9. Kleingruppen-Tageskarte Berlin BC", "10. Kleingruppen-Tageskarte Berlin ABC"};
    	double[] fahrkartenpreise = {2.90 , 3.30 , 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    
    	for (int i = 0; i < fahrkartenbezeichnung.length ; i++) {
    		System.out.printf("%s%s%.2f%s\n",  fahrkartenbezeichnung[i], " f�r ", fahrkartenpreise[i], " EUR ");
    	    }
    	
    	
    	System.out.printf("\nIhre Wahl: ");
    	
    	wahl = tastatur.nextInt();
    	while(wahl < 1 || wahl > fahrkartenbezeichnung.length) {
    		System.out.println("  >>falsche Eingabe<<");
    		System.out.printf("\nIhre Wahl: ");
    		wahl = tastatur.nextInt();
    	}
    	
    		int ticketpreisberechnung = wahl - 1;
    		ticketpreis = fahrkartenpreise[ticketpreisberechnung];
    	 	  	
    	System.out.print("Anzahl der Tickets: ");
    	anzahlTickets = tastatur.nextInt();
        if(anzahlTickets < 1 || anzahlTickets > 10) {
        	
        	anzahlTickets = 1;
        	System.out.println("Ung�ltige Ticketanzahl! Ticketanzahl wurde auf 1 gesetzt!");
        }
        
        return anzahlTickets * ticketpreis;
    	
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneM�nze;
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	//tastatur.close();
    	
    	return eingezahlterGesamtbetrag - zuZahlen;

    }
    
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
        	System.out.print("=");
        	warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgeld) {
    	
    	if(rueckgeld > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO ", rueckgeld);
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgeld >= 2.0) // 2 EURO-M�nzen
            {
         	 muenzeAusgeben(2, " EURO");
         	 rueckgeld -= 2.0;
            }
            while(rueckgeld >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1, " EURO");
         	 rueckgeld -= 1.0;
            }
            while(rueckgeld >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50, " CENT");
         	 rueckgeld -= 0.5;
            }
            while(rueckgeld >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20, " CENT");
         	 rueckgeld -= 0.2;
            }
            while(rueckgeld >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10, " CENT");
         	 rueckgeld -= 0.1;
            }
            while(rueckgeld >= 0.05)// 5 CENT-M�nzen
            {
            	muenzeAusgeben(5, " CENT");
         	 rueckgeld -= 0.05;
            }
        }
    }
    public static void warte(int millisekunde) {
    	
    	try {
        	Thread.sleep(millisekunde);
           	} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
           	}
    }
    
   public static void muenzeAusgeben(int betrag, String einheit) {
	   
	   System.out.println(betrag + einheit);
	   
   }
   private static boolean restart(){
		boolean restart = true;
		return restart;
	}
}