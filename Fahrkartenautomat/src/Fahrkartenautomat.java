﻿import java.util.Scanner;	

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       // Deklariert die Doublevariable zuZahlenderBetrag
       double zuZahlenderBetrag; 
       // Deklariert die Doublevariable eingezahlterGesamtbetrag    
       double eingezahlterGesamtbetrag;
       // Deklariert die Doublevariable eingeworfeneMünze
       double eingeworfeneMünze;
       // Deklariert die Doublevariable rückgabebetrag
       double rückgabebetrag;
       // Deklariert die ganzzahligen Variable anzahl
       byte anzahl;
 
       
       System.out.print("Zu zahlender Betrag (EURO): ");
    // Doublevariable wird initialisiert mit Scanner
       zuZahlenderBetrag = tastatur.nextDouble();
       
       System.out.print("Anzahl der Tickets: ");
    // Doublevariable wird initialisiert mit Scanner
       anzahl = tastatur.nextByte();
       
       // Geldeinwurf
       // -----------
    // Doublevariable wird initialisiert mit 0.0
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahl)
       {
  
    	   System.out.println("Noch zu zahlen: " + String.format("%.2f",zuZahlenderBetrag * anzahl - eingezahlterGesamtbetrag)  + " Euro");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	// Doublevariable wird initialisiert mit Scanner
    	   eingeworfeneMünze = tastatur.nextDouble();
    	// Doublevariable wird initialisiert mit neuen Werten, der zuvor eingefügten eingeworfeneMünze
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
    // Doublevariable Rückgabebetrag wird initialisiert mit subtraction von eingezahlterGesamtbetrag und zuZahlenderBetrag multipliziert mit der Anzahl der Fahrkarten
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag * anzahl;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " +  String.format("%.2f", rückgabebetrag) + " Euro");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2.00 Euro");
        	// Wert der Doublevariable Rückgabebetrag wird subtrahiert mit 2.00
	          rückgabebetrag -= 2.00;
           }
           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
           {
        	  System.out.println("1.00 Euro");
          	// Wert der Doublevariable Rückgabebetrag wird subtrahiert mit 1.00
	          rückgabebetrag -= 1.00;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("0.50 Euro");
          	// Wert der Doublevariable Rückgabebetrag wird subtrahiert mit 0.50
	          rückgabebetrag -= 0.50;
           }
           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
           {
        	  System.out.println("0.20 Euro");
          	// Wert der Doublevariable Rückgabebetrag wird subtrahiert mit 0.20
 	          rückgabebetrag -= 0.20;
           }
           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
           {
        	  System.out.println("0.10 Euro");
          	// Wert der Doublevariable Rückgabebetrag wird subtrahiert mit 0.10
	          rückgabebetrag -= 0.10;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("0.05 Euro");
          	// Wert der Doublevariable Rückgabebetrag wird subtrahiert mit 0.05
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}

// Aufgabepunkt 5
// Der Datentyp zum abfragen der Fahrkarten, ist Byte. Der Grund ist der, dass es unwahrscheinlich ist,
// dass mehr als 127 Fahrkarten insegsamt verkauft werden können. Eine Gleitkommavariable wurde nicht gewählt,
// da man NUR Ganze Fahrkarten kaufen kann.

// Aufgabenpunkt 6
// Bei der Berechnung (anzahl * kartenpreis), wird der Kartenpreis mit der Anzahl der Karten multipliziert,
// sodass der Korrekte KartenGesamtpreis errechnet werden kann.