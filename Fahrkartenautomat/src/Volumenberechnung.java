
public class Volumenberechnung {

	public static void main(String[] args) {
		
	
		System.out.print(wuerfel(2.2));
		System.out.print(quader(2.2, 1.5, 2.4));
		System.out.print(pyramide(2.2, 1.5));
	}
	
	
	public static double wuerfel(double a) {
		return a*a*a;
		
	}

	public static double quader(double a, double b, double c) {
		return a*b*c;
		
	}
	
	public static double pyramide(double h, double a) {
		return a*a*h/3.0;
		
	}
	public static double kugel(double r) {
		return 4/3*(r*r*r)*3.14;
		
	}

}
