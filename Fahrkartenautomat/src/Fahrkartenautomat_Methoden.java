import java.util.Scanner;

public class Fahrkartenautomat_Methoden {

	public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       
       double zuZahlen = fahrkartenbestellungErfassen();
       double bezahlen = fahrkartenBezahlen(zuZahlen);
       fahrkartenAusgabe();
       rueckgeldAusgeben(zuZahlen, bezahlen); 
    }
     
public static double fahrkartenbestellungErfassen() {
	Scanner tastatur = new Scanner(System.in);
	System.out.print("Zu zahlender Betrag (EURO): ");
    double doubleWert= tastatur.nextDouble();
    
    System.out.print("Anzahl der Tickets: ");
    int doubleWert2= tastatur.nextInt();
	return doubleWert * doubleWert2;
    }


public static double fahrkartenBezahlen(double zuZahlen) {
	Scanner tastatur = new Scanner(System.in);
	double eingezahlterGesamtbetrag = 0.0;
    while(eingezahlterGesamtbetrag < zuZahlen)
    {
 	   System.out.println("Noch zu zahlen: " + String.format("%.2f",zuZahlen - eingezahlterGesamtbetrag)  + " Euro");
 	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
 	   double eingeworfeneM�nze = tastatur.nextDouble();
       eingezahlterGesamtbetrag += eingeworfeneM�nze;   
    }
    return eingezahlterGesamtbetrag; 
}

public static void fahrkartenAusgabe() {
	System.out.println("\nFahrschein wird ausgegeben");
  for (int i = 0; i < 8; i++)
  {
     System.out.print("=");
     try {
		Thread.sleep(250);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  }
  System.out.println("\n\n");
}

public static double rueckgeldAusgeben(double zuZahlen, double eingezahlterGesamtbetrag) {
	double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
  if(r�ckgabebetrag > 0.0)
  {
	   System.out.println("Der R�ckgabebetrag in H�he von " +  String.format("%.2f", r�ckgabebetrag) + " Euro");
	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

      while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
      {
    	  System.out.println("2.00 Euro");
         r�ckgabebetrag -= 2.00;
      }
      while(r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
      {
   	  System.out.println("1.00 Euro");
         r�ckgabebetrag -= 1.00;
      }
      while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
      {
   	  System.out.println("0.50 Euro");
         r�ckgabebetrag -= 0.50;
      }
      while(r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
      {
   	  System.out.println("0.20 Euro");
          r�ckgabebetrag -= 0.20;
      }
      while(r�ckgabebetrag >= 0.10) // 10 CENT-M�nzen
     {
   	  System.out.println("0.10 Euro");
         r�ckgabebetrag -= 0.10;
      }
      while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
      {
   	  System.out.println("0.05 Euro");
          r�ckgabebetrag -= 0.05;
      }
  }

  System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                     "vor Fahrtantritt entwerten zu lassen!\n"+
                     "Wir w�nschen Ihnen eine gute Fahrt.");
return r�ckgabebetrag;
}


}