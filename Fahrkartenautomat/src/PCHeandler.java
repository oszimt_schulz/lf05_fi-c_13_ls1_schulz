import java.util.Scanner;

public class PCHeandler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		//System.out.println("was m�chten Sie bestellen?");
		//String artikel = myScanner.next();

		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();

		//System.out.println("Geben Sie den Nettopreis ein:");
		//double preis = myScanner.nextDouble();

		//System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//double mwst = myScanner.nextDouble();

		// Verarbeiten
		//double nettogesamtpreis = anzahl * preis;
		//double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		//System.out.println("\tRechnung");
		//System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		//System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
 String artikel = liesString();
 int anzahl = liesInt();
 double preis = liesDouble();
 double mwst = liesBrutto();
 double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
 double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
 rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
 
	}
	
	
	public static String liesString() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("was m�chten Sie bestellen?");
		String stringWert = myScanner.next();
		return stringWert;
		
	}
	
	public static int liesInt() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl ein:");
		int intWert = myScanner.nextInt();
		return intWert;
		
	}
	 
	public static double liesDouble() {
		@SuppressWarnings("resource")
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie den Nettopreis ein:");
		double doubleWert = myScanner.nextDouble();
		return doubleWert;
		
	}
	
	public static double liesBrutto() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie die MWST ein:");
		double doubleWert = myScanner.nextDouble();
		return doubleWert;
		
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		return anzahl * preis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
	}

}