//Erstellen Sie folgende Programme. Es werden jeweils 3 Zahlen eingegeben: 
//1. Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung 
//ausgegeben werden (If mit && / Und) 
//2. Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung 
//ausgegeben werden (If mit || / Oder) 
//3. Geben Sie die gr��te der 3 Zahlen aus. (If-Else mit && / Und) 

import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub 
		
	       Scanner zahlen = new Scanner(System.in);
	       
	       System.out.printf("Bitte erste Zahl eingeben");
	       double zahl1 = zahlen.nextDouble();
	       
	       System.out.printf("Bitte zweite Zahl eingeben");
	       double zahl2 = zahlen.nextDouble();
	       
	       System.out.printf("Bitte dritte Zahl eingeben");
	       double zahl3 = zahlen.nextDouble();
	       
	       
	       if (zahl1 > zahl2 && zahl3 > zahl1) {
	    	   System.out.printf(zahl1 + "ist die gr��te gew�hlte Zahl");
	       }
	       
	       else if (zahl3 > zahl2 || zahl3 > zahl1) { 
	    	   System.out.printf(zahl3 + "ist die gr��te gew�hlte Zahl");
	       }
	       
	       else {
	    	   System.out.printf(zahl2 + "ist die gr��te gew�hlte Zahl");

	       }
	    
	       
	 }
}


