import java.util.Scanner;

public class schleifen
{
    public static void main(String[] args)
    {
        // Aufgabe 1: Z�hlen
        increment(false);
        decrement(false); 

        //Aufgabe 2: Summe
        sum(false);
        sum2n(false);
        sum2n1(false); 
        
        //Aufgabe 3: Modulo
        modulo200(true); 
    }

    public static void increment(boolean flag)
    {
        if(flag == true)
        {
            System.out.print("Enter a number (increment): "); 
            @SuppressWarnings("resource")
            Scanner scanner = new Scanner(System.in); 
            int input = scanner.nextInt();
            for(int i = 1; i <= input; i++)
            {
                System.out.println(i); 
            }
        }
    }

    public static void decrement(boolean flag)
    {
        if(flag == true)
        {
            System.out.print("Enter a number (decrement): ");
            try(Scanner scanner = new Scanner(System.in))
            {
                int input = scanner.nextInt();
                for(int i = input; i > 0; i--)
                {
                    System.out.println(i); 
                }
            } 
        }
    }

    public static void sum(boolean flag)
    {
        if(flag == true)
        {
            System.out.print("Enter a number (sum): ");
            @SuppressWarnings("resource")
            Scanner scanner = new Scanner(System.in); 
            int input = scanner.nextInt(); 
            int sum = 0; 
            for(int i = 1; i <= input; i++)
            {
                sum += i; 
            }
            System.out.println("The sum is: " + sum); 
        }
    }
    
    public static void sum2n(boolean flag)
    {
        if(flag == true)
        {
            System.out.print("Enter a number (sum2n): ");
            @SuppressWarnings("resource")
            Scanner scanner = new Scanner(System.in);
            int input = scanner.nextInt();
            int sum = 0; 
            int i = 1; 
            while(i <= input)
            {
                if(i % 2 == 0)
                {
                    sum += i;  
                }
                i++;
            }
            System.out.println("The sum is (even): " + sum); 
        }
    }
    
    public static void sum2n1(boolean flag)
    {
        if(flag == true)
        {
            System.out.print("Enter a number (sum2n+1): ");
            try(Scanner scanner = new Scanner(System.in))
            {
                int input = scanner.nextInt();
                int sum = 0; 
                int i = 1;
                while(i <= input)
                {
                    if(i % 2 == 1)
                    {
                        sum += i; 
                    }
                    i++; 
                }
                System.out.println("The sum is (odd): " + sum);
            }
        }
    }
    
    public static void modulo200(boolean flag)
    {
        if(flag == true)
        { 
        	System.out.println("The number is divisible by 7: ");
            for(int i = 1; i <= 200; i++)
            {
                if(i % 7 == 0)
                {
                    System.out.print(i + ", ");
                }
            }
            
            System.out.println("\n\nThe number is not divisible by 5, but the number is divisible by 4: ");
            for(int i = 1; i <=200; i++) 
            {
                if(!(i % 5 == 0) && i % 4 == 0)
                {
                   System.out.print(i + ", "); 
                }
            }
        }
    }
}