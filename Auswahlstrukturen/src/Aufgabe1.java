

// Erstellen Sie folgende Programme. Es werden jeweils 2 Zahlen eingegeben: 
//1. Nennen Sie Wenn-Dann-Aktivit�ten aus ihrem Alltag. 
//2. Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden (If) 
//3. Wenn die 2. Zahl gr��er als die 1. Zahl ist, soll eine Meldung ausgegeben werden (If) 
//4. Wenn die 1. Zahl gr��er oder gleich als die 2. Zahl ist, soll eine Meldung ausgegeben 
//werden, ansonsten eine andere Meldung (If-Else)

//1. Wenn der Wecker klingelt, dann stehe ich auf.

public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int zahl1 = 4;
		int zahl2 = 3;
		
		if (zahl1 == zahl2) {
			System.out.println(zahl1 +" sind gleich "+zahl2);
		}
		if (zahl2 > zahl1) {
			System.out.println(zahl2 + " ist gr��er als " + zahl1);
		}
		if (zahl1 >= zahl2) {
			System.out.println(zahl1 + " ist gr��er oder gleich als " + zahl2);
		}
		else {
			System.out.println(" Es ist irgendwas anderes ");
		}
	}

}
